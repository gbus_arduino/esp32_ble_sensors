/*
  DHT Temperature/Humidity bluetooth sensor hub
  Use esp32 to provide multiple measurements from a number of DHT11/22 sensors.
  Configure the following, based on your wiring and preferences:
    - BLE_DEVICE_NAME, this name will be listed in the bluetooth scan list
    - NUM_SENSORS
    - dht[NUM_SENSORS] array, set gpio number and dht sensor type (in principle a mix of dht11/22 can be configured)
  Also, you must set your own C_UUIDs (https://www.uuidgenerator.net/)
    - SERVICE_UUID
    - C_UUIDs, make sure to generate one for each sensor
  Testing 4 sensors currently, but any number of sensors connected to the esp32 should work.
*/
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#include <DHT.h>
#include <DHT_U.h>


// Configuration starts here ===>>>
#define BLE_DEVICE_NAME  "Indoor TH sensors"
#define SERVICE_UUID        "7c74b185-69f7-4fd3-af51-beaa20353ffb"

#define NUM_SENSORS 4
DHT_Unified dht[] = {
  DHT_Unified(18, DHT11), 
  DHT_Unified(19, DHT11), 
  DHT_Unified(22, DHT11), 
  DHT_Unified(23, DHT11)
  };
const char *C_UUIDs[] = {
  "7bbab1d0-700c-407a-aa54-fe8f7cd54a00",
  "df184775-9bd7-4a6f-9d25-c0be6fb36fa5",
  "cb9d8f20-0c1f-4ad6-99a8-02da607352d0",
  "fdb8e7a9-5b73-41d9-bbf5-1788fa7911f7"
  };
// <<<=== End configuration

BLEServer* pServer = NULL;
BLECharacteristic* pCharacteristic[NUM_SENSORS] = {};
bool deviceConnected = false;
bool oldDeviceConnected = false;
uint32_t value = 0;
float prev_t[NUM_SENSORS];
float prev_h[NUM_SENSORS];

// Timer variables
unsigned long lastTime = 0;
unsigned long timerDelay = 30000;


class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
      BLEDevice::startAdvertising();
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};

void printSensorInfo(DHT_Unified d){
    // Print temperature sensor details.
  sensor_t sensor;
  d.temperature().getSensor(&sensor);
  Serial.println(F("------------------------------------"));
  Serial.println(F("Temperature Sensor"));
  Serial.print  (F("Sensor Type: ")); Serial.println(sensor.name);
  Serial.print  (F("Driver Ver:  ")); Serial.println(sensor.version);
  Serial.print  (F("Unique ID:   ")); Serial.println(sensor.sensor_id);
  Serial.print  (F("Max Value:   ")); Serial.print(sensor.max_value); Serial.println(F("°C"));
  Serial.print  (F("Min Value:   ")); Serial.print(sensor.min_value); Serial.println(F("°C"));
  Serial.print  (F("Resolution:  ")); Serial.print(sensor.resolution); Serial.println(F("°C"));
  Serial.println(F("------------------------------------"));
  // Print humidity sensor details.
  d.humidity().getSensor(&sensor);
  Serial.println(F("Humidity Sensor"));
  Serial.print  (F("Sensor Type: ")); Serial.println(sensor.name);
  Serial.print  (F("Driver Ver:  ")); Serial.println(sensor.version);
  Serial.print  (F("Unique ID:   ")); Serial.println(sensor.sensor_id);
  Serial.print  (F("Max Value:   ")); Serial.print(sensor.max_value); Serial.println(F("%"));
  Serial.print  (F("Min Value:   ")); Serial.print(sensor.min_value); Serial.println(F("%"));
  Serial.print  (F("Resolution:  ")); Serial.print(sensor.resolution); Serial.println(F("%"));
  Serial.println(F("------------------------------------"));
}


void updateTemp(int index, float temp){
  if(prev_t[index] != temp){
    prev_t[index] = temp;
  }
}


void updateHumidity(int index, float humidity){
  if(prev_h[index] != humidity){
    prev_h[index] = humidity;
  }
}


void setup() {
  Serial.begin(115200);

  // Create the BLE Device
  BLEDevice::init(BLE_DEVICE_NAME);

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Init sensors and BLD characteristics in one loop
  for (int i = 0; i < NUM_SENSORS; i++){
    // Init sensors
    dht[i].begin();
    printSensorInfo(dht[i]);
  
    // Create a BLE Characteristic
    pCharacteristic[i] = pService->createCharacteristic(
                        C_UUIDs[i],
                        BLECharacteristic::PROPERTY_READ   |
                        BLECharacteristic::PROPERTY_WRITE  |
                        BLECharacteristic::PROPERTY_NOTIFY |
                        BLECharacteristic::PROPERTY_INDICATE
                      );
    // https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.descriptor.gatt.client_characteristic_configuration.xml
    // Create a BLE Descriptor
    pCharacteristic[i]->addDescriptor(new BLE2902());
  }

  // Start the service
  pService->start();

  // Start advertising
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(false);
  pAdvertising->setMinPreferred(0x0);  // set value to 0x00 to not advertise this parameter
  BLEDevice::startAdvertising();
  Serial.println("Waiting a client connection to notify...");
}

void loop() {

  if ((millis() - lastTime) > timerDelay) {
    sensors_event_t event;
    for (int i = 0; i < NUM_SENSORS; i++) {
      dht[i].temperature().getEvent(&event);
      if (isnan(event.temperature)) {
        Serial.println(F("Error reading temperature!"));
      }
      else {
        updateTemp(i, event.temperature);
      }
      dht[i].humidity().getEvent(&event);
      if (isnan(event.relative_humidity)) {
        Serial.println(F("Error reading humidity!"));
      }
      else {
        updateHumidity(i, event.relative_humidity);
      }
    }

    // notify changed value
    if (deviceConnected) {
      for (int i = 0; i < NUM_SENSORS; i++) {
        String str = "";
        str += prev_t[i];
        str += ",";
        str += prev_h[i];
        pCharacteristic[i]->setValue((char*)str.c_str());
        pCharacteristic[i]->notify();

        Serial.print(F("Humidity: "));
        Serial.println(prev_h[i]);
        Serial.println(F("%"));

        Serial.print(F("Temperature: "));
        Serial.println(prev_t[i]);
        Serial.println(F("°C"));
        Serial.println(F("%  Temperature: "));
      }
      delay(30000);
    }

      // disconnecting
    oldDeviceConnected = deviceConnected;
    
    if (!deviceConnected && oldDeviceConnected) {
        delay(500); // give the bluetooth stack the chance to get things ready
        pServer->startAdvertising(); // restart advertising
        Serial.println("start advertising");
        oldDeviceConnected = deviceConnected;
    }
    // connecting
    if (deviceConnected && !oldDeviceConnected) {
        // do stuff here on connecting
        oldDeviceConnected = deviceConnected;
    }
    lastTime = millis();
  }
}
